package javafx;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class GamePiece extends Circle {

    private static final double RADIUS = 20.0;

    public GamePiece(double centerX, double centerY) {
        super(centerX, centerY, RADIUS);
        this.setStroke(Color.BLACK);
        this.setFill(Color.BLUE);
    }
}
