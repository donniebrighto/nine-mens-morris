package javafx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.shape.Circle;

public class FXMLController implements Initializable {

    @FXML
    private Circle field0;

    @FXML
    private Circle field1;

    @FXML
    private Circle field2;

    @FXML
    private Circle field3;

    @FXML
    private Circle field4;

    @FXML
    private Circle field5;

    @FXML
    private Circle field6;

    @FXML
    private Circle field7;

    @FXML
    private Circle field8;

    @FXML
    private Circle field9;

    @FXML
    private Circle field10;

    @FXML
    private Circle field11;

    @FXML
    private Circle field12;

    @FXML
    private Circle field13;

    @FXML
    private Circle field14;

    @FXML
    private Circle field15;

    @FXML
    private Circle field16;

    @FXML
    private Circle field17;

    @FXML
    private Circle field18;

    @FXML
    private Circle field19;

    @FXML
    private Circle field20;

    @FXML
    private Circle field21;

    @FXML
    private Circle field22;

    @FXML
    private Circle field23;

    @FXML
    private ChoiceBox<String> player1;

    @FXML
    private ChoiceBox<String> player2;

    @FXML
    private ChoiceBox<String> heuristic1;

    @FXML
    private ChoiceBox<String> heuristic2;

    @FXML
    private Button startButton;

    @FXML
    private CheckBox logging;

    @FXML
    private Label stageLabel;

    @FXML
    private Label playerLabel;


    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }


}
