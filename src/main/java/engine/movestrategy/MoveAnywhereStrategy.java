package engine.movestrategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.naming.OperationNotSupportedException;
import engine.Player;
import engine.board.Board;
import engine.board.Field;
import engine.board.Move;

public class MoveAnywhereStrategy implements MoveStrategy {

  @Override
  public List<Move> getPossibleMoves(Board board, Player player)
      throws OperationNotSupportedException {
    if (board.getPlayerPiecesNumber(player) > 3) {
      return Collections.emptyList();
    }
    return collectPossibleMoves(board, player);
  }

  private List<Move> collectPossibleMoves(Board board, Player player) {
    List<Move> possibleMoves = new ArrayList<>();
    List<Field> playerFields = board.getPlayerFields(player);
    for (Field source : playerFields) {
      addPossibleMoveForField(board, player, possibleMoves, source);
    }
    return possibleMoves;
  }

  private void addPossibleMoveForField(
      Board board, Player player, List<Move> possibleMoves, Field source) {
    for (Field destination : board.getFields()) {
      if (isFree(destination)) {
        possibleMoves.add(Move.normalMove(player, source, destination));
      }
    }
  }

  private boolean isFree(Field destination) {
    return destination.getOccupant().equals(Player.NONE);
  }
}
