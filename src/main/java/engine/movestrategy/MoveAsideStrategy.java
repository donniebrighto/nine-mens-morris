package engine.movestrategy;

import java.util.ArrayList;
import java.util.Collections;
import javax.naming.OperationNotSupportedException;
import engine.board.Board;
import engine.board.Field;
import engine.board.Move;
import engine.Player;

import java.util.List;

public class MoveAsideStrategy implements MoveStrategy {
  @Override
  public List<Move> getPossibleMoves(Board board, Player player) throws OperationNotSupportedException {
    if (board.getPlayerPiecesNumber(player) == 3) {
      return Collections.emptyList();
    }
    return collectPossibleMoves(player, board.getPlayerFields(player));
  }

  private ArrayList<Move> collectPossibleMoves(Player player, List<Field> filteredFields) {
    ArrayList<Move> possibleMoves = new ArrayList<>();
    for (Field field : filteredFields) {
      for (Field neighbour : field.getNeighbours()) {
        if (neighbour.getOccupant().equals(Player.NONE)) {
          possibleMoves.add(Move.normalMove(player, field, neighbour));
        }
      }
    }
    return possibleMoves;
  }

}
