package engine.movestrategy;

import javax.naming.OperationNotSupportedException;
import engine.board.Board;
import engine.board.Move;
import engine.Player;

import java.util.List;

public interface MoveStrategy {

    List<Move> getPossibleMoves(Board board, Player player) throws OperationNotSupportedException;

}
