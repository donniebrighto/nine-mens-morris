package engine.movestrategy;

import java.util.ArrayList;
import java.util.Collections;
import javax.naming.OperationNotSupportedException;
import engine.board.Board;
import engine.board.Field;
import engine.board.Move;
import engine.Player;

import java.util.List;

public class PlacingMoveStrategy implements MoveStrategy {

  private static final int FIRST_PHASE_MAX_ROUND = 9;

  @Override
  public List<Move> getPossibleMoves(Board board, Player player)
      throws OperationNotSupportedException {
    if (board.getPlayerRounds(player) == FIRST_PHASE_MAX_ROUND) {
      return Collections.emptyList();
    }
    return getFirstPhasePossibleMoves(player, board.getFields());
  }

  private ArrayList<Move> getFirstPhasePossibleMoves(Player player, Field[] fields) {
    ArrayList<Move> possibleMoves = new ArrayList<>();
    for (Field field : fields) {
      if (isFree(field)) {
        possibleMoves.add(Move.firstPhaseMove(player, field));
      }
    }
    return possibleMoves;
  }

  private boolean isFree(Field field) {
    return field.getOccupant().equals(Player.NONE);
  }
}
