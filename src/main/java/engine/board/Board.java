package engine.board;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import javax.naming.OperationNotSupportedException;
import engine.Player;

public class Board {

  private Field[] fields;
  private Line[] lines;
  private EnumMap<Player, Integer> playerRoundsMapping;
  private EnumMap<Player, Integer> playerPiecesMapping;
  private int evaluation;

  public Board(Field[] fields, Line[] lines) {
    this.fields = fields;
    this.lines = lines;
    initializeCounters();
  }

  private void initializeCounters() {
    playerRoundsMapping = new EnumMap<>(Player.class);
    playerPiecesMapping = new EnumMap<>(Player.class);
    playerRoundsMapping.put(Player.BLACK, 0);
    playerRoundsMapping.put(Player.WHITE, 0);
    playerPiecesMapping.put(Player.WHITE, 0);
    playerPiecesMapping.put(Player.BLACK, 0);
  }

  public List<Field> getPlayerFields(Player player) {
    ArrayList<Field> filteredFields = new ArrayList<>();
    for (Field field : fields) {
      if (field.getOccupant().equals(player)) {
        filteredFields.add(field);
      }
    }
    return filteredFields;
  }

  public int getPlayerRounds(Player player) throws OperationNotSupportedException {
    if(player.equals(Player.NONE)){
      throw new OperationNotSupportedException("Every move has to has player assigned");
    }
    return playerRoundsMapping.get(player);
  }

  public int getPlayerPiecesNumber(Player player) throws OperationNotSupportedException {
    if(player.equals(Player.NONE)){
      throw new OperationNotSupportedException("getPlayerPiecesNumber invoked to player other to BLACK or WHITE");
    }
    return playerPiecesMapping.get(player);
  }

  public void incrementCounter(Player player, Counter counter) {
    switch (counter){
      case PIECE:
        incrementCounter(player, playerPiecesMapping);
        break;
      case ROUND:
        incrementCounter(player, playerRoundsMapping);
        break;
    }
  }

  private void incrementCounter(Player player, EnumMap<Player, Integer> counterMapping) {
    Integer value = counterMapping.get(player);
    counterMapping.put(player, ++value);
  }

  public void decrementCounter(Player player, Counter counter) {
    switch (counter){
      case PIECE:
        decrementCounter(player, playerPiecesMapping);
        break;
      case ROUND:
        decrementCounter(player, playerRoundsMapping);
        break;
    }
  }

  public void decrementCounter(Player player, EnumMap<Player, Integer> counterMapping) {
    Integer value = counterMapping.get(player);
    counterMapping.put(player, --value);
  }

  public void evaluate() {
    // TODO
  }

  public int getEvaluation() {
    return evaluation;
  }

  public Field getField(int index) {
    return fields[index];
  }

  public Field[] getFields() {
    return fields;
  }

}
