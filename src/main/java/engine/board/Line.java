package engine.board;

import engine.Player;

public class Line {

  private static final int LINE_SIZE = 3;

  private Field[] fields;
  private boolean isMillUsed;

  public Line(Field... fields) {
    this.fields = fields;
    this.isMillUsed = false;
  }

  public Field[] getFields() {
    return fields;
  }

  public boolean isMillUsed() {
    return isMillUsed;
  }

  public boolean isCloseable(Player player, Field to) {
    for (Field field : fields) {
      if (otherFieldsNotOccupiedByPlayer(player, to, field)) {
        return false;
      }
    }
    return true;
  }

  private boolean otherFieldsNotOccupiedByPlayer(Player player, Field to, Field field) {
    return !field.equals(to) && !field.getOccupant().equals(player);
  }

  public boolean hasMill() {
    return fields[0].getOccupant().equals(fields[1].getOccupant())
        && fields[1].getOccupant().equals(fields[2].getOccupant());
  }
}
