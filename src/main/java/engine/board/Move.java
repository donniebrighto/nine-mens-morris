package engine.board;

import engine.Player;

public class Move {

  public static final Move PLAYER_WHITE_LOSE = new Move(Player.WHITE);
  public static final Move PLAYER_BLACK_LOSE = new Move(Player.BLACK);

  private Player player;
  private Field from;
  private Field to;
  private boolean isRemovingMove;
  private int evaluation;

  public static Move firstPhaseMove(Player current, Field to) {
    return new Move(current, to);
  }

  public static Move normalMove(Player player, Field from, Field to) {
    return new Move(player, from, to);
  }

  public static Move removingMove(Player playerWithMillClosed, Field from) {
    return new Move(playerWithMillClosed, from, true);
  }

  public static Move noMovesLeft(Player player) {
    return player.equals(Player.WHITE) ? PLAYER_WHITE_LOSE : PLAYER_BLACK_LOSE;
  }

  private Move(Player player, Field to) {
    this.player = player;
    this.to = to;
  }

  private Move(Player player, Field from, Field to) {
    this(player, to);
    this.from = from;
  }

  private Move(Player player, Field from, boolean isRemovingMove) {
    this.player = player;
    this.from = from;
    this.isRemovingMove = isRemovingMove;
  }

  private Move(Player player) {
    this.player = player;
  }

  public Player getPlayer() {
    return player;
  }

  public Field getFrom() {
    return from;
  }

  public Field getTo() {
    return to;
  }

  public boolean isRemovingMove() {
    return isRemovingMove;
  }

  public boolean hasClosedMill() {
    Line[] lines = to.getLines();
    return lines[0].isCloseable(player, to) || lines[1].isCloseable(player, to);
  }

  public int getEvaluation() {
    return evaluation;
  }

  public void setEvaluation(int evaluation) {
    this.evaluation = evaluation;
  }

  @Override
  public String toString() {
    return "Move{" +
        "player=" + player +
        ", from=" + from.getIndex() +
        ", to=" + to.getIndex() +
        '}';
  }
}
