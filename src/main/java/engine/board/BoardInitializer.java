package engine.board;

public final class BoardInitializer {

  /*
     REPRESENTATION OF BOARD
     0 - - - - - 1 - - - - - 2
     |   3 - - - 4 - - - 5   |
     |   |   6 - 7 - 8   |   |
     |   |   |       |   |   |
     9 - 10- 11      12- 13- 14
     |   |   |       |   |   |
     |   |   15- 16- 17  |   |
     |   18- - - 19- - - 20  |
     21- - - - - 22- - - - - 23
  */

  private static final int BOARD_SIZE = 24;
  private static final int NUMBER_OF_LINES_ON_BOARD = 16;

  private BoardInitializer(){}

  public static Board getInitialBoard() {
    Field[] fields = initialPositions();
    return new Board(fields, initBoardLines(fields));
  }

  private static Field[] initialPositions() {
    Field[] boardFields = new Field[BOARD_SIZE];
    initBoardFields(boardFields);
    initFieldsRelationships(boardFields);
    return boardFields;
  }

  private static void initFieldsRelationships(Field[] boardFields) {
    // first row
    boardFields[0].setNeighbours(boardFields[1], boardFields[9]);
    boardFields[1].setNeighbours(boardFields[0], boardFields[2], boardFields[4]);
    boardFields[2].setNeighbours(boardFields[1], boardFields[14]);
    // second row
    boardFields[3].setNeighbours(boardFields[4], boardFields[10]);
    boardFields[4].setNeighbours(
        boardFields[1], boardFields[3], boardFields[5], boardFields[7]);
    boardFields[5].setNeighbours(boardFields[4], boardFields[13]);
    // third row
    boardFields[6].setNeighbours(boardFields[11], boardFields[7]);
    boardFields[7].setNeighbours(boardFields[4], boardFields[6], boardFields[8]);
    boardFields[8].setNeighbours(boardFields[7], boardFields[12]);
    // fourth left row
    boardFields[9].setNeighbours(boardFields[0], boardFields[21], boardFields[10]);
    boardFields[10].setNeighbours(
        boardFields[9], boardFields[11], boardFields[3], boardFields[18]);
    boardFields[11].setNeighbours(boardFields[6], boardFields[15], boardFields[10]);
    // fourth right row
    boardFields[12].setNeighbours(boardFields[8], boardFields[13], boardFields[17]);
    boardFields[13].setNeighbours(
        boardFields[5], boardFields[12], boardFields[14], boardFields[20]);
    boardFields[14].setNeighbours(boardFields[2], boardFields[13], boardFields[23]);
    // fifth row
    boardFields[15].setNeighbours(boardFields[11], boardFields[16]);
    boardFields[16].setNeighbours(boardFields[15], boardFields[19], boardFields[17]);
    boardFields[17].setNeighbours(boardFields[12], boardFields[16]);
    // sixth row
    boardFields[18].setNeighbours(boardFields[10], boardFields[19]);
    boardFields[19].setNeighbours(
        boardFields[16], boardFields[18], boardFields[20], boardFields[22]);
    boardFields[20].setNeighbours(boardFields[13], boardFields[19]);
    // seventh row
    boardFields[21].setNeighbours(boardFields[9], boardFields[22]);
    boardFields[22].setNeighbours(boardFields[19], boardFields[21], boardFields[23]);
    boardFields[23].setNeighbours(boardFields[14], boardFields[22]);
  }

  private static Line[] initBoardLines(Field[] boardFields){
    Line[] lines = new Line[NUMBER_OF_LINES_ON_BOARD];
    // horizontal
    lines[0] = new Line(boardFields[0], boardFields[1], boardFields[2]);
    lines[1] = new Line(boardFields[3], boardFields[4], boardFields[5]);
    lines[2] = new Line(boardFields[6], boardFields[7], boardFields[8]);
    lines[3] = new Line(boardFields[9], boardFields[10], boardFields[11]);
    lines[4] = new Line(boardFields[12], boardFields[13], boardFields[14]);
    lines[5] = new Line(boardFields[15], boardFields[16], boardFields[17]);
    lines[6] = new Line(boardFields[18], boardFields[19], boardFields[20]);
    lines[7] = new Line(boardFields[21], boardFields[22], boardFields[23]);
    // vertical
    lines[8] = new Line(boardFields[0], boardFields[9], boardFields[21]);
    lines[9] = new Line(boardFields[3], boardFields[10], boardFields[18]);
    lines[10] = new Line(boardFields[6], boardFields[11], boardFields[15]);
    lines[11] = new Line(boardFields[1], boardFields[4], boardFields[7]);
    lines[12] = new Line(boardFields[16], boardFields[19], boardFields[22]);
    lines[13] = new Line(boardFields[8], boardFields[12], boardFields[17]);
    lines[14] = new Line(boardFields[5], boardFields[13], boardFields[20]);
    lines[15] = new Line(boardFields[2], boardFields[14], boardFields[23]);
    // set lines to fields (horizontal, vertical)
    boardFields[0].setLines(lines[0], lines[8]);
    boardFields[1].setLines(lines[0], lines[11]);
    boardFields[2].setLines(lines[0], lines[15]);

    boardFields[3].setLines(lines[1], lines[9]);
    boardFields[4].setLines(lines[1], lines[11]);
    boardFields[5].setLines(lines[1], lines[14]);

    boardFields[6].setLines(lines[2], lines[10]);
    boardFields[7].setLines(lines[2], lines[11]);
    boardFields[8].setLines(lines[2], lines[13]);

    boardFields[9].setLines(lines[3], lines[8]);
    boardFields[10].setLines(lines[3], lines[9]);
    boardFields[11].setLines(lines[3], lines[10]);

    boardFields[12].setLines(lines[4], lines[13]);
    boardFields[13].setLines(lines[4], lines[14]);
    boardFields[14].setLines(lines[4], lines[15]);

    boardFields[15].setLines(lines[5], lines[10]);
    boardFields[16].setLines(lines[5], lines[12]);
    boardFields[17].setLines(lines[5], lines[13]);

    boardFields[18].setLines(lines[6], lines[9]);
    boardFields[19].setLines(lines[6], lines[12]);
    boardFields[20].setLines(lines[6], lines[14]);

    boardFields[21].setLines(lines[7], lines[8]);
    boardFields[22].setLines(lines[7], lines[12]);
    boardFields[23].setLines(lines[7], lines[15]);
    return lines;
  }

  private static void initBoardFields(Field[] boardFields) {
    for (int i = 0; i < BOARD_SIZE; i++) {
      boardFields[i] = new Field(i);
    }
  }
}
