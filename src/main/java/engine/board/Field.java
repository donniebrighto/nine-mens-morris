package engine.board;

import engine.Player;

public class Field {

  private int index;
  private Player occupant;
  private Field[] neighbours;
  private Line[] lines;

  public Field(int index) {
    this.index = index;
    occupant = Player.NONE;
  }

  public void setOccupant(Player occupant) {
    this.occupant = occupant;
  }

  public void setNeighbours(Field... neighbours) {
    this.neighbours = neighbours;
  }

  public void setLines(Line... lines) {
    this.lines = lines;
  }

  public int getIndex() {
    return index;
  }

  public Player getOccupant() {
    return occupant;
  }

  public Field[] getNeighbours() {
    return neighbours;
  }

  public Line[] getLines() {
    return lines;
  }

  public boolean isInMill() {
    for(Line line : lines){
      if(line.hasMill()){
        return true;
      }
    }
    return false;
  }

  @Override
  public String toString() {
    return "Field{" +
        "index=" + index +
        ", occupant=" + occupant +
        '}';
  }
}
