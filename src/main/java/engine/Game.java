package engine;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.naming.OperationNotSupportedException;
import engine.movestrategy.MoveAnywhereStrategy;
import engine.movestrategy.PlacingMoveStrategy;
import engine.movestrategy.MoveAsideStrategy;
import engine.movestrategy.MoveStrategy;
import engine.board.Board;
import engine.board.BoardInitializer;
import engine.board.Counter;
import engine.board.Field;
import engine.board.Move;

public class Game {

  private Board board;
  private Map<GamePhase, MoveStrategy> gamePhaseMoveStrategyMapping;

  public Game() {
    board = BoardInitializer.getInitialBoard();
    initializeGamePhaseMoveStrategyMapping();
  }

  private void initializeGamePhaseMoveStrategyMapping() {
    gamePhaseMoveStrategyMapping = new EnumMap<>(GamePhase.class);
    gamePhaseMoveStrategyMapping.put(GamePhase.FIRST_PHASE, new PlacingMoveStrategy());
    gamePhaseMoveStrategyMapping.put(GamePhase.SECOND_PHASE, new MoveAsideStrategy());
    gamePhaseMoveStrategyMapping.put(GamePhase.THIRD_PHASE, new MoveAnywhereStrategy());
  }

  public GamePhase getGamePhase(Player player) throws OperationNotSupportedException {
    if (board.getPlayerRounds(player) < 9) {
      return GamePhase.FIRST_PHASE;
    } else if (board.getPlayerPiecesNumber(player) > 3) {
      return GamePhase.SECOND_PHASE;
    }
    return GamePhase.THIRD_PHASE;
  }

  public List<Move> getPossibleMoves(Player player) throws OperationNotSupportedException {
    return gamePhaseMoveStrategyMapping.get(getGamePhase(player)).getPossibleMoves(board, player);
  }

  public List<Field> getFieldsPossibleToRemoval(Player player) {
    Field[] fields = board.getFields();

    return Stream.of(fields)
        .filter(field -> field.getOccupant().equals(player))
        .filter(field -> !field.isInMill())
        .collect(Collectors.toCollection(ArrayList::new));
  }

  public boolean makeMoveAndCheckIfMillClosed(Move move) throws OperationNotSupportedException {
    validatePlayer(move);
    if (move.isRemovingMove()) {
      handleRemovingMove(move);
      return false;
    } else {
      handleNormalMove(move);
      return move.hasClosedMill();
    }
  }

  private void handleRemovingMove(Move move) {
    move.getFrom().setOccupant(Player.NONE);
    board.decrementCounter(opponent(move.getPlayer()), Counter.PIECE);
  }

  private Player opponent(Player player) {
    return player.equals(Player.BLACK) ? Player.WHITE : Player.BLACK;
  }

  private void handleNormalMove(Move move) {
    board.incrementCounter(move.getPlayer(), Counter.ROUND);
    incrementPieceCounterOrMakeSourceFieldAvailable(move);
    move.getTo().setOccupant(move.getPlayer());
  }

  private void incrementPieceCounterOrMakeSourceFieldAvailable(Move move) {
    if (moveFromFirstPhase(move)) {
      board.incrementCounter(move.getPlayer(), Counter.PIECE);
    } else {
      move.getFrom().setOccupant(Player.NONE);
    }
  }

  private void validatePlayer(Move move) throws OperationNotSupportedException {
    if (move.getPlayer() == Player.NONE) {
      throw new OperationNotSupportedException("Every move has to has player assigned");
    }
  }

  private boolean moveFromFirstPhase(Move move) {
    return move.getFrom() == null;
  }

  public void undoMove(Move move) throws OperationNotSupportedException {
    validatePlayer(move);
    if (move.isRemovingMove()) {
      handleUndoRemovingMove(move);
    } else {
      handleUndoNormalMove(move);
    }
  }

  private void handleUndoNormalMove(Move move) {
    decrementPieceCounterOrMakeDestinationFieldUnavailable(move);
    move.getTo().setOccupant(Player.NONE);
  }

  private void handleUndoRemovingMove(Move move) {
    Player opponent = opponent(move.getPlayer());
    move.getFrom().setOccupant(opponent);
    board.incrementCounter(opponent, Counter.PIECE);
  }

  private void decrementPieceCounterOrMakeDestinationFieldUnavailable(Move move) {
    if (moveFromFirstPhase(move)) {
      board.decrementCounter(move.getPlayer(), Counter.PIECE);
    } else {
      move.getFrom().setOccupant(move.getPlayer());
    }
  }

  public Board getBoard() {
    return board;
  }
}
