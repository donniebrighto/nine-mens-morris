package engine;

public enum GamePhase {
    FIRST_PHASE,
    SECOND_PHASE,
    THIRD_PHASE,
}
