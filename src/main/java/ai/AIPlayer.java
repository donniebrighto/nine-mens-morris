package ai;

import engine.Player;
import engine.board.Move;
import javax.naming.OperationNotSupportedException;

public interface AIPlayer {

  Move findBestMove(Player player) throws OperationNotSupportedException;

  Move getBestPieceToRemove(Player playerWhoClosedMill);

}
