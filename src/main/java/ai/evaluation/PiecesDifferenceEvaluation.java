package ai.evaluation;

import javax.naming.OperationNotSupportedException;
import engine.Player;
import engine.board.Board;

public class PiecesDifferenceEvaluation implements EvaluationStrategy {
  @Override
  public int evaluate(Board board) {
    try {
      return board.getPlayerPiecesNumber(Player.BLACK) - board.getPlayerPiecesNumber(Player.WHITE);
    } catch (OperationNotSupportedException e) {
      // TODO - obecna implementacja nie pozwala na rzucenie tego wyjatku dla podanego gracza,
      // jednak nalezaloby go lepiej obsluzyc
      e.printStackTrace();
      return 0;
    }
  }
}
