package ai.evaluation;

import engine.board.Board;

public interface EvaluationStrategy {

  int evaluate(Board board);

}
