package ai;

import ai.evaluation.EvaluationStrategy;
import java.util.List;
import javax.naming.OperationNotSupportedException;
import engine.Game;
import engine.Player;
import engine.board.Field;
import engine.board.Move;

public class MinMax implements AIPlayer {

  private EvaluationStrategy evaluation;
  private Game game;
  private int depth;

  private final Player MAX = Player.BLACK;
  private final Player MIN = Player.WHITE;

  public MinMax(Game game, EvaluationStrategy evaluation, int depth) {
    this.game = game;
    this.evaluation = evaluation;
    this.depth = depth;
  }

  @Override
  public Move findBestMove(Player current) throws OperationNotSupportedException {
    return minMax(depth, current, null);
  }

  public Move minMax(int depth, Player current, Move parent) throws OperationNotSupportedException {
    List<Move> possibleMoves = game.getPossibleMoves(current);
    if (depth == 0 || gameOver(possibleMoves)) {
      if(parent == null){
        return Move.noMovesLeft(current);
      }
      return getEvaluatedMove(current, parent, possibleMoves);
    }
    List<Move> resultMoves = evaluateMoves(depth, current, possibleMoves);
    return current.equals(MAX) ? max(resultMoves) : min(resultMoves);
  }

  private boolean gameOver(List<Move> possibleMoves) {
    return possibleMoves.isEmpty();
  }

  private List<Move> evaluateMoves(int depth, Player current, List<Move> possibleMoves)
      throws OperationNotSupportedException {
    for (Move move : possibleMoves) {
      Move removingMove = makeMoveAndGetPossibleRemovingToUndo(move);
      int evaluation = minMax(depth - 1, nextPlayer(current), move).getEvaluation();
      move.setEvaluation(evaluation);
      undoRemovingMoveIfExist(removingMove);
      game.undoMove(move);
    }
    return possibleMoves;
  }

  private Move makeMoveAndGetPossibleRemovingToUndo(Move move)
      throws OperationNotSupportedException {
    boolean hasMillClosed = game.makeMoveAndCheckIfMillClosed(move);
    Move removingMove = null;
    if (hasMillClosed) {
      removingMove = getBestPieceToRemove(move.getPlayer());
      removeIfPossible(removingMove);
    }
    return removingMove;
  }

  private void undoRemovingMoveIfExist(Move removingMove) throws OperationNotSupportedException {
    if (removingMove != null) {
      game.undoMove(removingMove);
    }
  }

  private void removeIfPossible(Move removingMove) throws OperationNotSupportedException {
    if (removingMove != null) {
      game.makeMoveAndCheckIfMillClosed(removingMove);
    }
  }

  private Move getEvaluatedMove(Player current, Move parent, List<Move> possibleMoves) {
    if (gameOver(possibleMoves)) {
      parent.setEvaluation(current.equals(MAX) ? Integer.MIN_VALUE : Integer.MAX_VALUE);
      return parent;
    }
    parent.setEvaluation(evaluation.evaluate(game.getBoard()));
    return parent;
  }

  private Move max(List<Move> possibleMoves) {
    int max = Integer.MIN_VALUE;
    Move bestMove = null;
    for (Move move : possibleMoves) {
      if (move.getEvaluation() > max) {
        max = move.getEvaluation();
        bestMove = move;
      }
    }
    return bestMove;
  }

  private Move min(List<Move> possibleMoves) {
    int min = Integer.MAX_VALUE;
    Move bestMove = null;
    for (Move move : possibleMoves) {
      if (move.getEvaluation() < min) {
        min = move.getEvaluation();
        bestMove = move;
      }
    }
    return bestMove;
  }

  @Override
  public Move getBestPieceToRemove(Player playerWhoClosedMill) {
    List<Field> fieldsPossibleToRemoval =
        game.getFieldsPossibleToRemoval(nextPlayer(playerWhoClosedMill));
    if (fieldsPossibleToRemoval.isEmpty()) {
      return null;
    }
    Field estimatedField = findFieldInTwoPiecesConfiguration(fieldsPossibleToRemoval);
    return estimatedField != null
        ? Move.removingMove(playerWhoClosedMill, estimatedField)
        : Move.removingMove(playerWhoClosedMill, fieldsPossibleToRemoval.get(0));
  }

  private Field findFieldInTwoPiecesConfiguration(List<Field> fieldsPossibleToRemoval) {
    Field estimatedField = null;
    for (Field field : fieldsPossibleToRemoval) {
      for (Field neighbour : field.getNeighbours()) {
        if (haveSameOccupant(field, neighbour)) {
          estimatedField = field;
          break;
        }
      }
    }
    return estimatedField;
  }

  private boolean haveSameOccupant(Field field, Field neighbour) {
    return field.getOccupant().equals(neighbour.getOccupant());
  }

  private Player nextPlayer(Player current) {
    return current.equals(Player.BLACK) ? Player.WHITE : Player.BLACK;
  }

  public int getDepth() {
    return depth;
  }

  public void setDepth(int depth) {
    this.depth = depth;
  }
}
