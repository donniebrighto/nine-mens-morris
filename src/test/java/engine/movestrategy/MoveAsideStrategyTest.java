package engine.movestrategy;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import javax.naming.OperationNotSupportedException;
import org.junit.Test;
import engine.Game;
import engine.board.Move;
import engine.Player;
import engine.board.Field;

public class MoveAsideStrategyTest {

  private MoveStrategy moveStrategy = new MoveAsideStrategy();

  @Test
  public void shouldReturnEmptyListForAllMovesBlocked() throws OperationNotSupportedException {
    Game game = new Game();
    setBlockedBoardState(game);

    List<Move> possibleMoves = moveStrategy.getPossibleMoves(game.getBoard(), Player.BLACK);

    assertEquals(0, possibleMoves.size());
  }

  private void setBlockedBoardState(Game game) {
    setOccupantForPositions(game, Player.BLACK, 0, 2, 21, 23);
    setOccupantForPositions(game, Player.WHITE, 1, 9, 14, 22);
  }

  @Test
  public void shouldReturnProperValueForGivenBoardState() throws OperationNotSupportedException {
    Game game = new Game();
    setOccupantForPositions(game, Player.BLACK, 0, 1, 2, 18, 23); // 5 expected
    setOccupantForPositions(game, Player.WHITE, 9, 3, 15, 22, 17); // 10 expected

    List<Move> possibleMovesForBlack = moveStrategy.getPossibleMoves(game.getBoard(), Player.BLACK);
    List<Move> possibleMovesForWhite = moveStrategy.getPossibleMoves(game.getBoard(), Player.WHITE);

    assertEquals(5, possibleMovesForBlack.size());
    assertEquals(10, possibleMovesForWhite.size());
  }

  @Test
  public void shouldReturnEmptyListForPlayerWithThreePiecesLeft()
      throws OperationNotSupportedException {
    Game game = new Game();
    setOccupantForPositions(game, Player.BLACK, 0, 1, 2);

    List<Move> possibleMoves = moveStrategy.getPossibleMoves(game.getBoard(), Player.BLACK);

    assertEquals(0, possibleMoves.size());
  }

  private void setOccupantForPositions(Game game, Player player, int... indexes) {
    Field[] fields = game.getBoard().getFields();
    Arrays.stream(indexes).forEach(inx -> {
      try {
        game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(player, fields[inx]));
      } catch (OperationNotSupportedException e) {
        System.err.println("setOccupantForPositions: operations not permitted for this player: " + player);
      }
    });
  }
}
