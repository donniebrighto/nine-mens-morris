package engine.movestrategy;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import java.util.Arrays;
import java.util.List;
import javax.naming.OperationNotSupportedException;
import org.junit.Test;
import engine.Game;
import engine.GamePhase;
import engine.Player;
import engine.board.Field;
import engine.board.Move;

public class MoveAnywhereStrategyTest {

  private MoveStrategy moveStrategy = new MoveAnywhereStrategy();

  @Test
  public void shouldReturnEighteenMovesForEachFieldForBothPlayerInThirdPhase()
      throws OperationNotSupportedException {
    Game game = new Game();
    Game spyGame = spy(game);
    doReturn(GamePhase.THIRD_PHASE).when(spyGame).getGamePhase(any());
    setOccupantForPositions(spyGame, Player.WHITE, 1, 2, 3);
    setOccupantForPositions(spyGame, Player.BLACK, 4, 18, 20);

    List<Move> possibleMovesForBlack = moveStrategy.getPossibleMoves(game.getBoard(), Player.BLACK);
    List<Move> possibleMovesForWhite = moveStrategy.getPossibleMoves(game.getBoard(), Player.WHITE);

    assertEquals(18 * 3, possibleMovesForBlack.size());
    assertEquals(18 * 3, possibleMovesForWhite.size());
  }

  @Test
  public void shouldReturnEmptyListForEachPlayerWithMoreThanThreePieces()
      throws OperationNotSupportedException {
    Game game = new Game();
    Game spyGame = spy(game);
    doReturn(GamePhase.THIRD_PHASE).when(spyGame).getGamePhase(any());
    setOccupantForPositions(spyGame, Player.WHITE, 1, 2, 3, 5);
    setOccupantForPositions(spyGame, Player.BLACK, 4, 18, 20, 21);

    List<Move> possibleMovesForBlack = moveStrategy.getPossibleMoves(game.getBoard(), Player.BLACK);
    List<Move> possibleMovesForWhite = moveStrategy.getPossibleMoves(game.getBoard(), Player.WHITE);

    assertEquals(0, possibleMovesForBlack.size());
    assertEquals(0, possibleMovesForWhite.size());
  }

  private void setOccupantForPositions(Game game, Player player, int... indexes) {
    Field[] fields = game.getBoard().getFields();
    Arrays.stream(indexes).forEach(inx -> {
      try {
        game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(player, fields[inx]));
      } catch (OperationNotSupportedException e) {
        System.err.println("setOccupantForPositions: operations not permitted for this player: " + player);
      }
    });
  }

}
