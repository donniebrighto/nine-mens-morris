package engine.movestrategy;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.stream.Stream;
import javax.naming.OperationNotSupportedException;
import org.junit.Test;
import engine.Game;
import engine.board.Board;
import engine.board.BoardInitializer;
import engine.board.Move;
import engine.Player;
import engine.board.Field;

public class PlacingMoveStrategyTest {

  private MoveStrategy moveStrategy = new PlacingMoveStrategy();

  @Test
  public void shouldReturnListOf24ElementsForAnyPlayerAndInitialBoard()
      throws OperationNotSupportedException {
    Board board = BoardInitializer.getInitialBoard();

    List<Move> possibleMovesBlack = moveStrategy.getPossibleMoves(board, Player.BLACK);
    List<Move> possibleMovesWhite = moveStrategy.getPossibleMoves(board, Player.WHITE);

    assertEquals(24, possibleMovesBlack.size());
    assertEquals(24, possibleMovesWhite.size());
  }

  @Test
  public void shouldReturnSixteenMovesForEightPiecesOnBoard()
      throws OperationNotSupportedException {
    Board board = BoardInitializer.getInitialBoard();
    Field[] fields = board.getFields();
    Stream.of(fields).limit(8).forEach(position -> position.setOccupant(Player.WHITE));

    List<Move> possibleMoves = moveStrategy.getPossibleMoves(board, Player.BLACK);

    assertEquals(16, possibleMoves.size());
  }

  @Test
  public void shouldReturnEmptyListForPlayerAfterNineRounds()
      throws OperationNotSupportedException {
    Game game = new Game();
    makeNineMovesForPlayer(game, Player.BLACK);

    List<Move> possibleMoves = moveStrategy.getPossibleMoves(game.getBoard(), Player.BLACK);

    assertEquals(0, possibleMoves.size());
  }

  private void makeNineMovesForPlayer(Game game, Player player)
      throws OperationNotSupportedException {
    Board board = game.getBoard();
    for(int i = 0; i < 9; i++){
      game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(player, board.getField(i)));
    }
  }
}
