package engine;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import javax.naming.OperationNotSupportedException;
import org.junit.Test;
import engine.board.Field;
import engine.board.Move;

public class GameTest {

  private Game game = new Game();

  @Test
  public void shouldReturnTrueIfMakingMoveCloseMill() throws OperationNotSupportedException {
    Field[] fields = game.getBoard().getFields();
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[0]));
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[1]));

    boolean hasClosedMill =
        game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[2]));

    assertTrue(hasClosedMill);
  }

  @Test
  public void shouldReturnFalseIfMakingMoveThatDoesNotCloseMill()
      throws OperationNotSupportedException {
    Field[] fields = game.getBoard().getFields();

    boolean hasClosedMill =
        game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[2]));

    assertFalse(hasClosedMill);
  }

  @Test
  public void shouldReturnEmptyListForPlayerWithAllPiecesInMills()
      throws OperationNotSupportedException {
    Field[] fields = game.getBoard().getFields();
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[0]));
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[1]));
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[2]));
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[3]));
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[4]));
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[5]));

    List<Field> possibleForRemoval = game.getFieldsPossibleToRemoval(Player.BLACK);

    assertEquals(0, possibleForRemoval.size());
  }

  @Test
  public void shouldReturnThreeFieldsForPlayerWithOneMillAndThreePieces()
      throws OperationNotSupportedException {
    Field[] fields = game.getBoard().getFields();
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[0]));
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[1]));
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[2]));
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[9]));
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[4]));
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[5]));
    Field[] expectedResult = new Field[] {fields[4], fields[5], fields[9]};

    List<Field> possibleForRemoval = game.getFieldsPossibleToRemoval(Player.BLACK);

    assertArrayEquals(expectedResult, possibleForRemoval.toArray());
  }

  @Test
  public void shouldReturnSixForPlayerWithNoMillAndSixPieces()
      throws OperationNotSupportedException {
    Field[] fields = game.getBoard().getFields();
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[0]));
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[22]));
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[2]));
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[3]));
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[18]));
    game.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(Player.BLACK, fields[5]));

    List<Field> possibleForRemoval = game.getFieldsPossibleToRemoval(Player.BLACK);

    assertEquals(6, possibleForRemoval.size());
  }
}
