package ai;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import ai.evaluation.EvaluationStrategy;
import ai.evaluation.PiecesDifferenceEvaluation;
import java.util.Arrays;
import javax.naming.OperationNotSupportedException;
import org.junit.Before;
import org.junit.Test;
import engine.Game;
import engine.GamePhase;
import engine.Player;
import engine.board.Field;
import engine.board.Move;

public class MinMaxTest {

  private Game game;
  private EvaluationStrategy evaluation;
  private MinMax minMax;

  @Before
  public void init(){
    game = new Game();
    evaluation = new PiecesDifferenceEvaluation();
  }

  @Test
  public void shouldReturnWinningMoveIfItIsPossibleInNextTurn()
      throws OperationNotSupportedException {
    Game spyGame = spy(game);
    doReturn(GamePhase.SECOND_PHASE).when(spyGame).getGamePhase(Player.BLACK);
    doReturn(GamePhase.THIRD_PHASE).when(spyGame).getGamePhase(Player.WHITE);
    minMax = new MinMax(spyGame, evaluation, 2);
    makeMoves(spyGame, Player.BLACK, 0, 1, 14, 22);
    makeMoves(spyGame, Player.WHITE, 3, 15, 20);

    Move move = minMax.findBestMove(Player.BLACK);

    assertEquals(2, move.getTo().getIndex());
    assertEquals(14, move.getFrom().getIndex());
  }

  @Test
  public void shouldReturnClosingMillMoveIfItIsPossibleInNextTurn()
      throws OperationNotSupportedException {
    Game spyGame = spy(game);
    doReturn(GamePhase.SECOND_PHASE).when(spyGame).getGamePhase(Player.BLACK);
    doReturn(GamePhase.SECOND_PHASE).when(spyGame).getGamePhase(Player.WHITE);
    minMax = new MinMax(spyGame, evaluation, 3);
    makeMoves(spyGame, Player.BLACK, 21, 22, 14, 3, 5, 10);
    makeMoves(spyGame, Player.WHITE, 1, 4, 18, 20, 2, 8);

    Move move = minMax.findBestMove(Player.BLACK);

    assertEquals(23, move.getTo().getIndex());
    assertEquals(14, move.getFrom().getIndex());
  }

  @Test
  public void shouldReturn_PLAYER_WHITE_LOSE_ObjectIfPlayerWhiteHasNoMoreMoves()
      throws OperationNotSupportedException {
    Game spyGame = spy(game);
    doReturn(GamePhase.SECOND_PHASE).when(spyGame).getGamePhase(Player.BLACK);
    doReturn(GamePhase.SECOND_PHASE).when(spyGame).getGamePhase(Player.WHITE);
    minMax = new MinMax(spyGame, evaluation, 2);
    makeMoves(spyGame, Player.BLACK, 9, 4, 13, 14);
    makeMoves(spyGame, Player.WHITE, 0, 1, 2, 5);

    Move move = minMax.findBestMove(Player.WHITE);

    assertEquals(Move.PLAYER_WHITE_LOSE, move);
  }

  private void makeMoves(Game spyGame, Player player, int... indexes) throws OperationNotSupportedException {
    Field[] fields = spyGame.getBoard().getFields();
    Arrays.stream(indexes).forEach(index -> {
      try {
        spyGame.makeMoveAndCheckIfMillClosed(Move.firstPhaseMove(player, fields[index]));
      } catch (OperationNotSupportedException e) {
        e.printStackTrace();
      }
    });
  }
}
